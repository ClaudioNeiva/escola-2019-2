package br.ucsal.bes20192.testequalidade.escola.tui;

import java.io.PrintStream;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TuiHelperUnitarioTest {

	@Mock
	private Scanner scannerMock;

	@InjectMocks
	private TuiHelper tuiHelper;

//	@Before
//	public void setup() {
//		scannerMock = Mockito.mock(Scanner.class);
//		tuiHelper = new TuiHelper();
//		tuiHelper.scanner = scannerMock;
//	}

	/**
	 * Verificar a obten��o do nome completo. Caso de teste: primeiro nome "Claudio"
	 * e sobrenome "Neiva" resulta no nome "Claudio Neiva".
	 */
	@Test
	public void testarObterNomeCompleto() {
		// Dados de entrada
		String nome = "Claudio";
		String sobrenome = "Neiva";

		Mockito.when(scannerMock.nextLine()).thenReturn(nome).thenReturn(sobrenome);

		// Sa�da esperada
		String nomeCompletoEsperado = "Claudio Neiva";

		// Executar o m�todo sob teste e obter o resultado atual
		String nomeCompletoAtual = tuiHelper.obterNomeCompleto();

		// Comprar o resultado esperado com o resultado atual
		Assert.assertEquals(nomeCompletoEsperado, nomeCompletoAtual);
	}

	/**
	 * Verificar a obten��o exibi��o de mensagem. Caso de teste: mensagem "Tem que
	 * estudar." resulta em "Bom dia! Tem que estudar.".
	 */
	@Test
	public void testarExibirMensagem() {
		// Dados de entrada
		String mensagem = "Tem que estudar.";

		// Sa�da esperada. N�o ser� poss�vel verificar diretamente o resultado esperado,
		// ent�o, ap�s ao exibirMenasgem, utilizaremos o mock para verficar o
		// comportamento esperado.
		// String resutadoEsperado = "Bom dia! Tem que estudar.";

		// Antes de executar o m�todo exibirMensagem � necess�rio mocar o objeto
		// apontado por System.out
		PrintStream printStreamMock = Mockito.mock(PrintStream.class);
		System.setOut(printStreamMock);

		// Executar o m�todo que est� sendo testado, no caso exibirMensagem, que tem
		// retorno void, n�o sendo poss�vel obter, neste momento, o resultadoAtual.
		tuiHelper.exibirMensagem(mensagem);

		// Como a intera��o entre o exibirMensagem e o usu�rio, via console, foi
		// substituida pela intera��o via printStreamMock, � poss�vel executar
		// verifica��es das intera��es com este mock.
		Mockito.verify(printStreamMock).print("Bom dia! ");
		Mockito.verify(printStreamMock).println(mensagem);
	}

}

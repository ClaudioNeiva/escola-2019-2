package br.ucsal.bes20192.testequalidade.escola.stub;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

public class AlunoDAOStub extends AlunoDAO {

	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		if (matricula.equals(567)) {
			return AlunoBuilder.umAluno().nascidoEm(2003).build();
		}
		throw new IllegalArgumentException("Matricula " + matricula + " n�o configuada no AlunoDAOStub.");
	}

}

package br.ucsal.bes20192.testequalidade.escola.mock;

import java.util.HashMap;
import java.util.Map;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

public class AlunoDAOMock extends AlunoDAO {

	// Map<nome-do-metodo-chamado, par�metro-passado-metodo-chamada>
	private Map<String, Object> chamadasMetodos = new HashMap<String, Object>();

	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		if (matricula.equals(567)) {
			return AlunoBuilder.umAluno().nascidoEm(2003).build();
		}
		throw new IllegalArgumentException("Matricula " + matricula + " n�o configuada no AlunoDAOStub.");
	}

	@Override
	public void salvar(Aluno aluno) {
		chamadasMetodos.put("salvar", aluno);
	}

	public boolean verificar(String nomeMetodo, Object objeto) {
		if (chamadasMetodos.containsKey(nomeMetodo) && chamadasMetodos.get(nomeMetodo).equals(objeto)) {
			return true;
		}
		return false;
	}
}

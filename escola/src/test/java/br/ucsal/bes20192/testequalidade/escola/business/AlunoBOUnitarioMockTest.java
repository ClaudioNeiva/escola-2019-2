package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.mock.AlunoDAOMock;
import br.ucsal.bes20192.testequalidade.escola.stub.DateHelperStub;

public class AlunoBOUnitarioMockTest {

	private static AlunoDAOMock alunoDAOMock;
	private static DateHelperStub dateUtilStub;
	private static AlunoBO alunoBO;

	@BeforeClass
	public static void setupClass() {
		alunoDAOMock = new AlunoDAOMock();
		dateUtilStub = new DateHelperStub();
		alunoBO = new AlunoBO(alunoDAOMock, dateUtilStub);
	}

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter� 16
	 * anos.
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		// Dados de entrada - matr�cula de um aluno conhecido pelo Stub que tenha sido
		// configurado como nascido em 2003
		Integer matricula = 567;

		// Sa�da esperada
		Integer idadeEsperada = 16;

		// Executar o m�todo calculo da idade e obter a sa�da atual
		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(idadeEsperada, idadeAtual);
	}

	/**
	 * Verificar se alunos ativos s�o atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {
		// Dados de entrada
		Aluno aluno = AlunoBuilder.umAlunoAtivo().build();

		// Sa�da esperada - � que o salvar do AlunoDAO tenha sido chamado

		// Executar o m�todo atualizar e obter a sa�da atual, como o m�todo
		// alunoBO.atualizar � do tipo command (tem retorno void), n�o ser� poss�vel
		// obter o "resultado atual".
		alunoBO.atualizar(aluno);

		// Verificar se ocorreu uma chamada ao AlunoDAO.salvar(aluno).
		Assert.assertTrue(alunoDAOMock.verificar("salvar", aluno));
	}

}

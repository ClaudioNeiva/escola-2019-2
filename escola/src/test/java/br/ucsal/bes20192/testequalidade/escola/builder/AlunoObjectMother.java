package br.ucsal.bes20192.testequalidade.escola.builder;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;

//FIXME Remover, ela et� aqui apenas para ilustrar um caminho, mas esse racioc�nio foi pra dentro do AlunoBuilder.
public class AlunoObjectMother {

	public static Aluno umAlunoMaiorDeIdade() {
		Aluno aluno1 = new Aluno();
		aluno1.setMatricula(123);
		aluno1.setNome("Claudio");
		aluno1.setSituacao(SituacaoAluno.ATIVO);
		aluno1.setAnoNascimento(1950);
		return aluno1;
	}

	public static Aluno umAlunoAtivo() {
		Aluno aluno1 = new Aluno();
		aluno1.setMatricula(123);
		aluno1.setNome("Claudio");
		aluno1.setSituacao(SituacaoAluno.ATIVO);
		aluno1.setAnoNascimento(2003);
		return aluno1;
	}

	public static Aluno umAlunoCancelado() {
		Aluno aluno1 = new Aluno();
		aluno1.setMatricula(123);
		aluno1.setNome("Claudio");
		aluno1.setSituacao(SituacaoAluno.CANCELADO);
		aluno1.setAnoNascimento(2003);
		return aluno1;
	}

}

package br.ucsal.bes20192.testequalidade.escola.business;

//import static org.mockito.Mockito.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

@RunWith(MockitoJUnitRunner.class)
public class AlunoBOUnitarioMockitoTest {

	@Mock
	private AlunoDAO alunoDAOMock;

	@Mock
	private DateHelper dateUtilMock;

	// A classe que est� sendo testada n�o deve ser mocada!
	@InjectMocks
	private AlunoBO alunoBO;

	// @Before
	// public void setup() {

	// MockitoAnnotations.initMocks(this);

	// alunoDAOMock = Mockito.mock(AlunoDAO.class);
	// dateUtilMock = Mockito.mock(DateHelper.class);

	// � necess�rio instanciar completamente a classe que est� sendo testada.
	// Tamb�m � poss�vel instanci�-la "parcialmente" (spy) para os casos onde um
	// m�todo da classe sob teste depende de outro m�todo na mesma classe.
	// alunoBO = new AlunoBO(alunoDAOMock, dateUtilMock);
	// }

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter� 16
	 * anos.
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		// Dados de entrada: um aluno nascido em 2003
		Integer matricula = 123;
		Aluno aluno = AlunoBuilder.umAluno().comMatricula(matricula).nascidoEm(2003).build();
		Mockito.when(alunoDAOMock.encontrarPorMatricula(matricula)).thenReturn(aluno);

		// Dados de entrada: o ano atual � 2019, para que possamos ter 16 anos para
		// algu�m nascido em 2003.
		Mockito.when(dateUtilMock.obterAnoAtual()).thenReturn(2019);

		// Sa�da esperada:
		Integer idadeEsperada = 16;

		// Executar o m�todo sob teste e obter a sa�da atual.
		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		// Verificar o resultado esperado em rela��o ao atual
		Assert.assertEquals(idadeEsperada, idadeAtual);
	}

	/**
	 * Verificar se alunos ativos s�o atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {
		// Dados de entrada: um aluno ativo
		Aluno aluno = AlunoBuilder.umAlunoAtivo().build();

		// Executar o m�todo sob teste (que n�o tem retorno)
		alunoBO.atualizar(aluno);

		// Como comparar o resultado esperado com o resultado atual, se o m�todo que
		// est� sendo testado � do tipo command, ou seja, n�o tem retorno (void).
		// � nesse momento que o mock apresenta funcionalidade adicional em rela��o ao
		// stub;
		// A forma de verificar o comportamento adequado do m�todo
		// alunoBO.atualizar(aluno) � observar se ocorreu a chamada ao m�todo
		// alunoDAO.salvar(aluno).
		Mockito.verify(alunoDAOMock).salvar(aluno);

	}

}

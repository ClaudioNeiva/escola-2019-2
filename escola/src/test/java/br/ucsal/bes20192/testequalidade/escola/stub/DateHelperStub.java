package br.ucsal.bes20192.testequalidade.escola.stub;

import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class DateHelperStub extends DateHelper {

	@Override
	public Integer obterAnoAtual() {
		return 2019;
	}
	
}

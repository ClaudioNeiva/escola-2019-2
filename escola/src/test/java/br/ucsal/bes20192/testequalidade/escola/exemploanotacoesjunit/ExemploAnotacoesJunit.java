package br.ucsal.bes20192.testequalidade.escola.exemploanotacoesjunit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ExemploAnotacoesJunit {

	@BeforeClass
	public static void setupClasse() {
		System.out.println("setupClasse");
	}
	
	@Before
	public void setup() {
		System.out.println("\tsetup");
	}
	
	@After
	public void teardown() {
		System.out.println("\tteardown");
	}

	@AfterClass
	public static void teardownClasse() {
		System.out.println("teardownClasse");
	}

	@Test
	public void test1() {
		System.out.println("\t\ttest1");
	}

	@Test
	public void test2() {
		System.out.println("\t\ttest2");
	}
	
	@Test
	public void test3() {
		System.out.println("\t\ttest3");
	}
	
}

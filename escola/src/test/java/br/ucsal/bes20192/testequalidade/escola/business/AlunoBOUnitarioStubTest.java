package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.stub.AlunoDAOStub;
import br.ucsal.bes20192.testequalidade.escola.stub.DateHelperStub;

public class AlunoBOUnitarioStubTest {

	private static AlunoDAOStub alunoDAOStub;
	private static DateHelperStub dateUtilStub;
	private static AlunoBO alunoBO;

	@BeforeClass
	public static void setupClass() {
		alunoDAOStub = new AlunoDAOStub();
		dateUtilStub = new DateHelperStub();
		alunoBO = new AlunoBO(alunoDAOStub, dateUtilStub);
	}

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter� 16
	 * anos.
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		// Dados de entrada - matr�cula de um aluno conhecido pelo Stub que tenha sido
		// configurado como nascido em 2003
		Integer matricula = 567;

		// Sa�da esperada
		Integer idadeEsperada = 16;

		// Executar o m�todo calculo da idade e obter a sa�da atual
		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(idadeEsperada, idadeAtual);
	}

	/**
	 * Verificar se alunos ativos s�o atualizados.
	 */
	public void testarAtualizacaoAlunosAtivos() {
		// Os stubs n�o resolvem o problema deste teste.
		// Este teste simplesmente n�o consegue verificar nada
		// Dados de entrada
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().build();

		// Sa�da esperada - � que o objeto tenha sido persistido na base

		// Executar o m�todo atualizar e obter a sa�da atual, como o m�todo
		// alunoBO.atualizar � do tipo command (tem retorno void), n�o ser� poss�vel
		// obter o "resultado atual".
		alunoBO.atualizar(alunoEsperado);

		// Comparar o resultado esperado com o resultado atual
		// Aqui dever�amos ser capazes de questionar o AlunoDAO sobre a chamada do
		// m�todo salvar

	}

}

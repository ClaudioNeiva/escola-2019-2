package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class AlunoBOIntegradoTest {

	private static AlunoDAO alunoDAO;
	private static DateHelper dateUtil;
	private static AlunoBO alunoBO;

	@BeforeClass
	public static void setupClass() {
		alunoDAO = new AlunoDAO();
		dateUtil = new DateHelper();
		alunoBO = new AlunoBO(alunoDAO, dateUtil);
	}

	@Before
	public void setup() {
		// Aqui dever�amos utilizar um framework para limpar a base. DBUnit?
		alunoDAO.excluirTodos();
	}

	@AfterClass
	public static void tearDownClass() {
		// Aqui dever�amos utilizar um framework para limpar a base. DBUnit?
		alunoDAO.excluirTodos();
	}

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter� 16
	 * anos. Caso de teste # | entrada | sa�da esperada 1 | aluno nascido em 2003 |
	 * 16
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		// Dados de entrada
		Aluno aluno1 = AlunoBuilder.umAluno().nascidoEm(2003).build();
		alunoDAO.salvar(aluno1);

		// Sa�da esperada
		Integer idadeEsperada = 16;

		// Executar o m�todo calculo da idade e obter a sa�da atual
		Integer idadeAtual = alunoBO.calcularIdade(aluno1.getMatricula());

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(idadeEsperada, idadeAtual);
	}

	/**
	 * Verificar se alunos ativos s�o atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {
		// Dados de entrada
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().build();

		// Sa�da esperada - � que o objeto tenha sido persistido na base

		// Executar o m�todo calculo da idade e obter a sa�da atual
		alunoBO.atualizar(alunoEsperado);
		Aluno alunoAtual = alunoDAO.encontrarPorMatricula(alunoEsperado.getMatricula());

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(alunoEsperado, alunoAtual);
	}

	@Test
	public void exemploSemUsoDoMasDoBuilder() {
		AlunoBuilder alunoBuilder = AlunoBuilder.umAluno().comMatricula(123).comNome("Maria").nascidoEm(2000).ativo();
		Aluno aluno1 = alunoBuilder.build();
		Aluno aluno2 = alunoBuilder.comNome("Pedro").build();
		Aluno aluno3 = alunoBuilder.nascidoEm(1950).build();
		Aluno aluno4 = alunoBuilder.cancelado().build();
		System.out.println("aluno1=" + aluno1); // 123 | Maria | 2000 | ATIVO		//123 | Maria | 2000 | ATIVO
		System.out.println("aluno2=" + aluno2); // 123 | Pedro | 2000 | ATIVO		//123 | Pedro | 2000 | ATIVO
		System.out.println("aluno3=" + aluno3); // 123 | Maria | 1950 | ATIVO		//123 | Pedro | 1950 | ATIVO
		System.out.println("aluno4=" + aluno4); // 123 | Maria | 2000 | CANCELADO	//123 | Pedro | 1950 | CANCELADO
	}

	@Test
	public void exemploComUsoDoMasDoBuilder() {
		AlunoBuilder alunoBuilder = AlunoBuilder.umAluno().comMatricula(123).comNome("Maria").nascidoEm(2000).ativo();
		Aluno aluno1 = alunoBuilder.build();
		Aluno aluno2 = alunoBuilder.mas().comNome("Pedro").build();
		Aluno aluno3 = alunoBuilder.mas().nascidoEm(1950).build();
		Aluno aluno4 = alunoBuilder.mas().cancelado().build();
		System.out.println("aluno1=" + aluno1); // 123 | Maria | 2000 | ATIVO		//123 | Maria | 2000 | ATIVO
		System.out.println("aluno2=" + aluno2); // 123 | Pedro | 2000 | ATIVO		//123 | Pedro | 2000 | ATIVO
		System.out.println("aluno3=" + aluno3); // 123 | Maria | 1950 | ATIVO		//123 | Pedro | 1950 | ATIVO
		System.out.println("aluno4=" + aluno4); // 123 | Maria | 2000 | CANCELADO	//123 | Pedro | 1950 | CANCELADO
	}

}
